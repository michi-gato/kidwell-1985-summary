# title page
* Osiris journal was founded in 1936 by George Sarton and relaunched by the History of Science Society
* It is an annual thematic journal highlighting significan themes in the history of science
* The theme in 1985 when today's article was published was "Historical Writing on American Science" under the sub-section of "Newer Areas" alongside articles about Scienc & Technology, or Science & War, or Science & Public Policy since WWII
* Google Scholar notes that this article has only been cited 21 times :/

Sources: 
* https://hssonline.org/
* https://www.journals.uchicago.edu/journals/osiris/about 


# Clara Sue Kidwell
* academic scholar, historian, feminist
* member of the White Earth Chippewa peoples (aka northern Minnesota) and of Choctaw descent (aka Alabama, Florida, Mississippi, and Louisiana)
* University of Oklahoma Professor Emeritus. Where she also received her Bachelors in Letters (1963), Masters with a fellowship in the history of science (1966), and PhD in History of Science (1970)
* Served as Assistant Director of Cultural Resources at the National Museum of the American Indian, Smithsonian Institution in early 1990s
* Previously held Professor positions at University of California at Berkeley (1974-95); Dartmouth College, Hanover, New Hampshire (1980); University of Minnesota (1972-74); Haskell Indian Junior College in Lawrence, Kansas (1970-72); Kansas City Art Institute (1968-69); Director of the Native American Studies program and Professor of History at the University of Oklahoma
* Founder (2007) and Director of the American Indian Center at the University of North Carolina at Chapel Hill
  * increasing programs that address education, health and child welfare for tribes who have been unable to qualify for federal recognition with the Bureau of Indian Affairs 
* Select Publications include
  * "Native Americans: Restoring the Power of Thought Woman" for the anthology, " Sisterhood Is Forever: The Women's Anthology for a New Millennium" (2003)
  * Treasures of The National Museum of The American Indian By W. Richard West Charlotte Heth Richard W. Hill Clara Sue Kidwell (2004)
  * A Native American Theology; By Clara Sue Kidwell, Homer Noley, George E. Tinkerob-- about traditional categories of Christian systematic theology (Creation, Deity, Christology, etc.), each of these is reimagined consistent with Native experience, values, and worldview
  * The Vanishing Native: Reappears in the College Curriculum (1991)

Sources:
* http://www.ncaihb.org/board-clara-sue-kidwell.php
* https://wdchumanities.org/dcdm/items/show/1673
* https://diversity.cofc.edu/journal-articles/the-vanishing-native

# Background: Indigenous Knowledge
* I'm not going to dive too deep into the background of indigenous knowledge since thise article is essentially about that but I will say that there are other terms that this domain of studies is referred to as. 
* In this article, Kidwell refers to this as Native American Science, often seen within Native American Studies departments or within Anthropology departments. Throughout my education, I've known this domain as indigenous knowledge or indigenous ways of knowing so I will refer to it as that from here on out
* Other terms that I will use or that Kidwell mentioned
  * turtle island is the term that many Algonquian- and Iroquoian-speaking Indigenous peoples use when referring to North America. Came from oral histories that tell stories of a turtle that supports and holds the world on its back. Turtle is considered an icon of life 
  * in Ojibwe, the peoples that Kidwell is a descendent of, Earth is completely flooded because The Creator had cleansed the world of feuding peoples in order to begin life anew. Only some animals survived the flood and they were tasked to recreate the world by swimming deep beneath the water to gather soil. 

Source:
* https://www.thecanadianencyclopedia.ca/en/article/turtle-island

# Main Themes and RQs
* Ways in which European and indigenous thought changed/diverged over time
* There are common characteristics of European and indigenous scientific activities:
  1. observation of a body of physical phenomena existing apart from human beings
  2. desire to control those phenomena and the forces behind them
  3. attempt to exercise that control
DIFFERENCES
* Differentiations "European science was increasingly based in concepts of lawful behavior of natural forces. Native American beliefs were based on the willful behavior of those forces. The one presupposed rational ways of understanding those laws; the other sought interaction with the forces of nature through dreams, visions, and ceremonies-through intuitive and personal ways of comprehending and controlling those forces." (210)
* Euro was about experimentation, getting similar results to enhance predictive power of science
* Indigenous concept = personal relationships of groups or individuals with spirit forces is necessary to assure desired results, and thus ceremony and ritual take the place of experiment.
* Illustration of Ojibew peoples performing a snowshoe dance. danced with the snowshoes under the feet, at the falling of the first snow in the beginning of winter, when they sing a song of thanksgiving to the Great Spirit for sending them a return of snow, when they can run on their snowshoes in their valued hunts, and easily take the game for their food.”

PROBLEMS
* Little to no written language, oral histories and artifacts lost outside of European/American province of history
* Historians relying on Indigenous sources = interpretation problems
* And even relying on sources writen about native people by European observers = problems stemming from European contact itself
* "Contact has led to the loss of native knowledge rather than its advances" -- Historians focus on "change" as the natural subject -- does not enlighten about thenature of Indigenous agriculture or rituals but raises issues of the result of European contact on Indigenous practices (211)
* ethnographic rather than historical -- synchronic rather than diachronic -- looking at picture of a cultrue at one point in time rather than providing a sense of change or development in the native systems of knowledge"

Source:
* https://en.wikipedia.org/wiki/Ojibwe#/media/File:Ojibwa_dance.jpg
* https://www.brainerddispatch.com/business/small-business/4824082-Ojibway-snowshoe-making-brings-pride-to-those-who-can-master-knots-lacing

# Methods

Source:
* https://books0977.tumblr.com/post/61760748244/native-american-mother-and-child-reading-life

# Supporting Evidence
1. Ethnoscience = method in anthropology examines boundaries of categories in systems of classifcation (reminds me of boundary objects from Bowker & Starr?). Used to analyze observations of indigenous peopels and define physical phenomena (212)
  * relationship between objective reality and indigenous cognitive structures for seeing the world 
  * William Stutevant: defined ethnoscience as a method for the study of "the system of knowledge and cognition typical of a given culture" (212)
  * linguistic analysis
  * gather information from indigenous informants
  * "discover" native principles (interesting use of discover)
  * etic approach
  * "reveal ways in wichi indigenous people organize, understand, and control their world views" (214)

2. Archaeostronomy and Ethnoastronomy
* astronomy =  science based on observation rather than experiment
* Archaeology = Archaeologists have presented orientation of structures as objective evidence for the observational powers of native people
* Athony Aveni describes this as Astronomy  for the nonastronomer the movements of celestial bodies as they appear to the earthbound observer as he looked at the Mayan calendar system (215)
* Other scholars who looked at archaeoastronomy: J. E. S. Thompson for the Mayan calendar written in codex about lunar and planet eclipses; Gregory Severin for zodiacs and eclipse
* Researchers like Alice Kehoe looked at rock and boulder formations and shadows to signify evidence of human construction efforts around marking north and south, sun rising, or seasonal solstices (Even though indigenous people denied the correlations)

3. Ethnobotany
* early observers treated the Indians as subjects rather than scientists, they include much important information about Indian uses of the environment, particularly plants
* Francisco Hernandez (physician, Spain): produced a major compendium of descriptions of plants, including Aztec knowledge of their forms and uses. (early 1600s)
* Fra Bernardino Sahagun, a Franciscan priest: compiled material from native Aztecs. (early 1800)
* John Josselyn: New England's Rarities Discovered recorded Indian uses of plants
* Le Page du Pratz: Indians were described among the plants and fishes and animals of lower Mississippi Valley region
* William Bartram, English naturalist: most systematic and scientific observer of flora and fauna (including indians)
* long tradition of studies contained the prefix "ethno" with some appended scientific category: Ethnogeography; Ethnoentornology; Ethnobiology; Ethnozoology; 
* collaboration between ethnographers and botanists
* lead to observing how Indigenous people use plants for medicinal practices and curing. Issues with these herbal remedy practices being seen as incredulous (trickery) or too credulous, claiming knowledge of marvelous cures have been lost 

4. Agriculture
* Health influenced by: diet and development of stable food supplies through agriculture
* Richard Yarnell observed archaeological techniques to identify plant remains and to indicate their role in the diet of local indigenous peoples and believes people may have domesticated plants like sunflowers
* Paul Manglesdor studied genetic relationships between different types of corn (teosinte, tripsacum, and domesticated corn) 
* Henry T. Lewis and Calvin Martin learned how indigenous agricultural techniques included burning areas to clear fields and to promote the growth of wild plants
* Handful of researchers learned more about field cultivation and water managment to make agriculture possible. 

5. Technology
* Last is technology! 
* interesting that this paper started with human activity and control and prediction as science but on p. 226, Kidwell mentions "human activity focuses on control of the environment rather than prediction of changes in it, it is usually called technology rather than science"
* Similar to engineers, native knowledge was generally more practical than theoretical.
* Indigenous technology focuses on weaving techniques, metalworking (e.g. shaping gold) as reflections of cultural values associated with those forms
* knotting and sophisticated weaving of cords represent technology and mathematical practices in Inca practice of record-keeping
* these technologies represent how Symbol, myth, and ritual important to indigenous culture than american society

# Arguements and Contributions
* aggregation and summary of the domains that have come out ethnographic studies about Indigenous knowledge and peoples. This include ethnohistory, ethnoscience, and the various ethnobotanies,  ethnogeographies, ethnoentomologies, and other "ethno" studies 
* studies have often been purely descriptive of the ways in which indigenous people perceived, how they predicted events in, and controlled their environments. Not much analysis
* Biggest critique of these studies highlight the diachronic and synchronic approaches to indigenous knowledge with ethnographic and scientific methods
* realize that the activities of native people can be judged by criteria that do not necessarily reflect only the Greco-Roman, Judeo-Christian, Western scientific world view of modern society (227)
* see last Future Directions section for helpful bibliographies to learn more about ethno-specific studies: 
  * George Murdock has major source of bibliographic information on ethnographic studies of North American tribes
  * Mark Barrow, Jerry Niswander, and Robert Fortuine for medicine bibliographies
  * Richard Ford for indigenous ethnobotany
  * Stephen McCluskey's article for Archaeoastronomy and Ethnoastronomy
* study of the history of native American science can meld archaeology, anthropology, and history and can enrich the history of science with a way of seeing culture as a system of belief that influences the responses of people to the understanding of the natural phenomena around them  

Source: https://www.researchgate.net/figure/Elements-of-traditional-Ojibwe-interactions-with-the-northern-Great-Lakes-environment_fig3_226008742

# Discussion Qs

Sychronic: events of a particular time, without reference to their historical context
Diacrhonic: study of a phenomenon (especially language) as it changes through time

Source: https://www.muskogeephoenix.com/archives/kidwell-to-set-up-museum-studies-degree-program-at-bacone/article_2026a1bb-154f-5323-a723-b5829914fec1.html (2011)
